#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <dirent.h>
#include <time.h>
#include <stdbool.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <syslog.h>

#define MAX_GAMBAR 15
#define DELAY_BETWEEN_GAMBAR 5
#define DELAY_BETWEEN_FOLDER 30

void execute_killer(char filename[]){
    pid_t cid = fork();

    if (cid < 0) 
    {
        exit(EXIT_FAILURE);
    }
    if (cid == 0){
        char *argv[] = {"gcc", filename, "-o", "killer", NULL};
        execv("/bin/gcc", argv);
    }

    int status;
    wait (&status);
}


void file_killer(int pid_main, char *proses, char *mode_run){
    FILE *fp;
    char filename[] = "killer.c";

    fp= fopen(filename, "w");
    fprintf(fp, "#include <stdio.h>\n#include <stdlib.h>\n#include <signal.h>\n");
    fprintf(fp, "#include <unistd.h>\n#include <sys/types.h>\n#include <sys/wait.h>\n\n");

    fprintf(fp, "void hapus_program(char filename[]) {\n\tpid_t child_id = fork();\n\n");
    fprintf(fp, "\tif (child_id < 0) {\n\t\texit(EXIT_FAILURE);\n\t}\n\n");
    fprintf(fp, "\tif (child_id == 0) {\n\t\tchar *argv[] = {\"rm\", filename, NULL};\n\t\texecv(\"/bin/rm\", argv);\n\t}\n\n");
    fprintf(fp, "\tint status;\n\twait(&status);\n}\n\n");

    if (strcmp(mode_run, "-a") == 0) {
        fprintf(fp, "void pkill(char file_proses[]) {\n\tpid_t child_id= fork();\n\n");
        fprintf(fp, "\tif (child_id < 0) {\n\t\texit(EXIT_FAILURE);\n\t}\n\n");
        fprintf(fp, "\tif (child_id == 0) {\n\t\tchar *argv[] = {\"pkill\", \"-9\", \"-f\", file_proses, NULL};\n\t\texecv(\"/bin/pkill\", argv);\n\t}\n\n");
        fprintf(fp, "\twaitpid(child_id, NULL, 0);\n}\n\n");

        fprintf(fp, "int main() {\n\tpkill(\"%s\");\n\n", proses);
        fprintf(fp, "\thapus_program(\"killer\");\n\t//hapus_program(\"killer.c\");\n\n\treturn 0;\n}");
    } 
    else if(strcmp(mode_run, "-b") == 0) {
        fprintf(fp, "int main() {\n\tint pid = %d;\n\tint terminate = kill(pid, SIGKILL);\n\n", pid_main);
        fprintf(fp, "\thapus_program(\"killer\");\n\t//hapus_program(\"killer.c\");\n\n\treturn 0;\n}");
    }

    fclose(fp);

    execute_killer(filename);

}

void nge_zip(char *filename){
    pid_t child_id;
    child_id = fork();

    if (child_id < 0) {
        exit(EXIT_FAILURE);
    }

    if (child_id == 0) {

        char nama_dir[25];
        strcpy(nama_dir, filename);
        strcat(filename, ".zip");

        char *argv[] = {"zip", "-9rmq", filename, nama_dir, NULL};
        execv("/bin/zip", argv);
    }
}


void download_gambar(char url[], char foldername[]){
    pid_t cid = fork();
    char nama_folder[60];
    strcpy(nama_folder, foldername);

    if (cid < 0) {
        exit(EXIT_FAILURE);
    }

    if (cid == 0){
        struct tm *tlocal;
        time_t waktu_dtk = time(NULL);
        tlocal = localtime(&waktu_dtk);

        char filename[25];
        int ukuran = (waktu_dtk%1000)+50;
        char str_ukuran[5];

        snprintf(str_ukuran, 5, "%d", ukuran);
        strcat(url, str_ukuran);
        strftime(filename, 25, "/%Y-%m-%d_%H:%M:%S.jpg", tlocal);
        strcat(foldername, filename);
        
        char *argv[] = {"wget", "-qO", foldername, url, NULL};
            execv("/bin/wget", argv);
    }
}


void buat_directory(char *url, char foldername[]) {
    pid_t cid;
    cid = fork();

    if (cid < 0) {
        exit(EXIT_FAILURE);
    }
    if (cid == 0) {
    char *argv[] = {"mkdir", "-p", foldername, NULL};
        execv("/bin/mkdir", argv);
    }

    waitpid(cid, NULL, 0);

    pid_t ncid = fork();
    if (ncid < 0) {
        exit(EXIT_FAILURE);
    }
    if (ncid == 0) {

    int i = 0;
        while(1) {
            if(i==MAX_GAMBAR) {
                nge_zip(foldername);
                break;
            }
            
            download_gambar(url, foldername);

            i++;
            sleep(DELAY_BETWEEN_GAMBAR);
        }

        exit(EXIT_SUCCESS);
    }
}





int main(int argc, char *argv[]){
        if (argc != 2 || (strcmp(argv[1], "-a") != 0 && strcmp(argv[1], "-b") != 0)) {
        printf("Usage: %s [-a|-b]\n", argv[0]);
        return 1;
    }

    pid_t pid, sid;
    pid = fork();
    char url[] = "https://picsum.photos/";

    if (pid > 0) {
        // buat program "killer"
        file_killer(pid, argv[0], argv[1]);

        exit(EXIT_SUCCESS);
    }

    if (pid < 0) {
        exit(EXIT_FAILURE);
    }

    umask(0);
    sid = setsid();

    if (sid < 0) {
        exit(EXIT_FAILURE);
    }

    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);

    while(1){
        struct tm *local;
        time_t t = time(NULL);
        local = localtime(&t);
        char foldername[256];
        strftime(foldername, 30, "%Y-%m-%d_%H:%M:%S", local);
        buat_directory(url,foldername);
         sleep(DELAY_BETWEEN_FOLDER);
    }

    return 0;

}
