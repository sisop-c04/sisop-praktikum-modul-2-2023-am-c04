# sisop-praktikum-modul-2-2023-AM-C04
Terdiri dari:
- Alfadito Aulia Denova | 5025211157 
- Layyinatul Fuadah | 5025211207 
- Armadya Hermawan S | 5025211243 



## Penjelasan Soal dan Jawaban
### Soal 1
Pada soal nomor 1 dijelaskan bahwa Grape-kun adalah seorang penjaga hewan di kebun binatang, yang bertugas untuk melakukan penjagaan pada beberapa hewan-hewan yang ada di kebun binatang sebelum melakukan penjagaan Grape-kun harus mengetahui terlebih dahulu hewan apa aja yang harus dijaga dalam drive kebun binatang tersebut terdapat folder gambar dari hewan apa saja yang harus dijaga oleh Grape-kun. 

**Point A**
Grape-kun harus mendownload file tersebut untuk disimpan pada penyimpanan local komputernya. Dan untuk melakukan melihat file gambar pada folder yang telah didownload Grape-kun harus melakukan unzip pada folder tersebut.

```R
int main(){
    download("https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq", "binatang.zip");
    unzip("binatang.zip");
    getFile(".");
generateDirectory();
    moveAnimal(".", "HewanDarat", "HewanAir", "HewanAmphibi");
    zipAnimal();
}
```
Script tersebut terdiri dari enam function call yang dieksekusi dalam fungsi main(). Berikut penjelasan masing-masing function call:
1.	Fungsi download menerima dua parameter yaitu URL file yang ingin didownload dan nama file output untuk file yang didownload. Dalam script tersebut, fungsi download digunakan untuk mendownload file binatang.zip dari URL https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq dan menyimpannya dengan nama binatang.zip.
2.	Fungsi unzip menerima satu parameter yaitu nama file zip yang ingin diekstrak. Dalam script tersebut, fungsi unzip digunakan untuk mengekstrak file binatang.zip yang telah didownload pada langkah sebelumnya.
3.	Fungsi getFile menerima satu parameter yaitu path dari direktori yang ingin ditampilkan daftar filenya. Dalam script tersebut, fungsi getFile digunakan untuk menampilkan daftar file pada direktori saat ini, yaitu direktori tempat file binatang.zip telah diekstrak.
4.	Fungsi generateDirectory digunakan untuk membuat tiga direktori, yaitu HewanDarat, HewanAir, dan HewanAmphibi.   Direktori-direktori ini digunakan untuk memilah file-file gambar hewan yang telah diekstrak pada langkah sebelumnya.
5.	Fungsi moveAnimal digunakan untuk memindahkan file-file gambar hewan ke dalam direktori yang sesuai dengan habitatnya, yaitu HewanDarat, HewanAir, atau HewanAmphibi. Fungsi ini menerima empat parameter, yaitu path direktori sumber,
path direktori HewanDarat, path direktori HewanAir, dan path direktori HewanAmphibi.
6.	Fungsi zipAnimal digunakan untuk melakukan kompresi terhadap tiga direktori yang telah dibuat pada langkah pertama. Fungsi ini akan mengkompresi tiga direktori tersebut menjadi satu file zip yang disimpan dengan nama "binatang-compressed.zip".

```R
void download(char *url, char *namefile){
    int stat;
    pid_t downloadID = fork();

    if (downloadID == 0){
    char *args[] = {"wget", "--no-check-certificate", url, "-q", "-O", namefile, NULL};
    execv("/opt/homebrew/bin/wget", args);
    }
    waitpid(downloadID, &stat, 0);
}
```
Fungsi `download` digunakan untuk mengunduh file dari URL yang diberikan dan menyimpannya dengan nama yang ditentukan. Ia menggunakan `fork()` untuk membuat proses child, yang kemudian menggunakan `execv()` untuk menjalankan perintah `wget` dengan argumen untuk mengunduh file. Argumen yang diteruskan ke `wget` adalah:
`--no-check-certificate`: jangan centang sertifikat SSL/TLS
`url`: URL file yang akan diunduh
`-q`: mode diam, tidak menampilkan informasi kemajuan atau status
`-O`: file keluaran, nama file untuk menyimpan unduhan sebagai
`NULL`: akhir dari daftar argumen
Proses parent menunggu proses child selesai menggunakan `waitpid()`.

**Point B**
Setelah berhasil melakukan unzip Grape-kun melakukan pemilihan secara acak pada file gambar tersebut untuk melakukan shift penjagaan pada hewan tersebut.

```R
void unzip(char *sourceDir){
    int stat;
    pid_t unzipID = fork();

    if (unzipID == 0){
    char *args[] = {"unzip", "-q", sourceDir, NULL};
    execv("/usr/bin/unzip", args);
    }

    waitpid(unzipID, &stat, 0);
}
```

Fungsi `unzip` digunakan untuk mengekstrak semua file dari arsip ZIP di direktori yang ditentukan. Ini menggunakan `fork()` untuk membuat proses child, yang kemudian menggunakan `execv()` untuk menjalankan perintah `unzip` dengan argumen untuk mengekstrak file. Argumen yang diteruskan ke `unzip` adalah:
`-q`: mode diam, tidak menampilkan informasi kemajuan atau status
`sourceDir`: nama file arsip ZIP yang akan diekstrak
`NULL`: akhir dari daftar argumen
Proses parent menunggu proses child selesai menggunakan `waitpid()`.

```R
void getFile(char *directory){
    int stat;
  char *path[100];

  struct dirent *dp;
  DIR *folder;
    srand(time(NULL));
    folder = opendir(directory);
```
Fungsi `getFile` digunakan untuk membuat daftar semua file di direktori yang ditentukan. Ini pertama kali membuka direktori menggunakan `opendir()`. Kemudian ia menggunakan `readdir()` untuk mengulangi setiap file dalam direktori. Untuk setiap file, ia memeriksa apakah itu file biasa (bukan direktori atau file khusus) menggunakan `S_ISREG()`. Jika ya, itu menambahkan nama file ke `path` array.

```R
if (folder != NULL){
    int i = 0;
    while ((dp = readdir(folder)) != NULL){
        if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0 && strstr(dp->d_name, ".jpg") != NULL){
        if (dp->d_type == DT_REG){
          char *token = dp->d_name;
            path[i] = token;
            i++;
        }
```
Dalam fungsi `getFile`, kode memeriksa apakah direktori yang ditentukan dalam parameter dapat dibuka menggunakan fungsi `opendir`. Jika dapat dibuka, ia menelusuri entri direktori menggunakan fungsi `readdir` untuk menemukan file dengan ekstensi ".jpg" menggunakan fungsi `strstr`. Jika entri adalah file biasa, itu akan menyimpan nama file di array `path`.
Fungsi `strcmp` digunakan untuk memeriksa apakah entri bukan "." atau ".." karena itu adalah entri khusus dalam direktori yang masing-masing mewakili direktori saat ini dan direktori induk.

```R
int size = sizeof(*path);
    int random = rand() % size;

    // Create file txt
    FILE *file;
    file = fopen("penjaga.txt", "w");
    fprintf(file, "Hewan Yang Dijaga : %s", strtok(path[random], "."));
    fclose(file);
    closedir(folder);
    }
}
```
script selanjutnya akan memilih secara acak salah satu file gambar dan menyimpan nama file tersebut pada sebuah file teks baru dengan nama "penjaga.txt".

**Point C**
Karena Grape-kun adalah orang yang perfeksionis Grape-kun ingin membuat direktori untuk memilah file gambar tersebut. Direktori tersebut dengan nama HewanDarat, HewanAmphibi, dan HewanAir. Setelah membuat direktori tersebut Grape-kun harus melakukan filter atau pemindahan file gambar hewan sesuai dengan tempat tinggal nya.

```R
void generateDirectory(){
    int stat;
    id_t child_id;

    if ((child_id = fork()) == 0){
    char *args[] = {"mkdir", "-p", "HewanDarat", NULL};
    execv("/bin/mkdir", args);
    }
    while ((wait(&stat)) > 0);
        if ((child_id = fork()) == 0){
    char *args[] = {"mkdir", "-p", "HewanAir", NULL};
    execv("/bin/mkdir", args);
    }
    while ((wait(&stat)) > 0);
    if ((child_id = fork()) == 0){
    char *args[] = {"mkdir", "-p", "HewanAmphibi", NULL};
    execv("/bin/mkdir", args);
    }
    while ((wait(&stat)) > 0);

    waitpid(child_id, &stat, 0);
}

```
sebuah fungsi bernama `generateDirectory()` yang berfungsi untuk membuat tiga direktori dengan menggunakan perintah `mkdir`. Direktori tersebut akan diberi nama "HewanDarat", "HewanAir", dan "HewanAmphibi". Fungsi ini menggunakan proses fork untuk memisahkan proses yang akan menjalankan setiap perintah `mkdir` dan melakukan `wait` untuk menunggu proses sebelumnya selesai sebelum melanjutkan ke perintah selanjutnya. Setelah ketiga perintah `mkdir` selesai dieksekusi, fungsi ini menunggu hingga proses terakhir selesai dengan menggunakan `waitpid`.

```R
void moveAnimal(char *soruce, char *des1, char *des2, char *des3){
    int stat;
    id_t child_id;
  struct dirent *dp;
  DIR *folder;
    folder = opendir(soruce);

    if (folder != NULL){
    while ((dp = readdir(folder)) != NULL){
        if (dp->d_type == DT_REG)
        {
        // if match with darat
        if (strstr(dp->d_name, "darat") != NULL){
            if ((child_id = fork()) == 0){
            char *argv[] = {"mv", dp->d_name, des1, NULL};
            execv("/bin/mv", argv);
            }
            while ((wait(&stat)) > 0)
            ;
        }
        if (strstr(dp->d_name, "air") != NULL){
            if ((child_id = fork()) == 0){
            char *argv[] = {"mv", dp->d_name, des2, NULL};
            execv("/bin/mv", argv);
            }
            while ((wait(&stat)) > 0)
            ;
        }
        if (strstr(dp->d_name, "amphibi") != NULL){
            if ((child_id = fork()) == 0){
            char *argv[] = {"mv", dp->d_name, des3, NULL};
            execv("/bin/mv", argv);
            }
            while ((wait(&stat)) > 0)
            ;
        }
        }
    }
    closedir(folder);
    }
    waitpid(child_id, &stat, 0);
}
```
moveAnimal yang berfungsi untuk memindahkan file-file hewan yang sudah diunduh dan diextract ke direktori yang sesuai dengan jenis hewan tersebut. Fungsi ini menerima empat parameter yaitu `source` yang merupakan direktori sumber atau direktori tempat file-file hewan diunduh dan diextract, `des1` yang merupakan direktori tujuan untuk hewan darat, `des2` untuk hewan air, dan `des3` untuk hewan amphibi.
Pertama, fungsi membuka direktori `source` menggunakan `opendir`. Kemudian, dilakukan perulangan dengan menggunakan `readdir` untuk membaca setiap file yang ada di direktori source. Jika jenis file yang dibaca adalah regular file `(dp->d_type == DT_REG)`, maka dilakukan pengecekan jenis hewan dengan menggunakan strstr pada nama file` (dp->d_name)`.
Jika nama file mengandung kata "darat", maka file tersebut akan dipindahkan ke direktori `des1 `menggunakan `mv` dengan melakukan fork dan `execv`. Proses wait dilakukan setelah child process selesai melakukan pemindahan file agar parent process menunggu hingga child process selesai. Langkah yang sama dilakukan pada file yang memiliki kata "air" dan "amphibi" dengan memindahkannya ke direktori `des2` dan `des3` masing-masing.
Setelah semua file dipindahkan ke direktori tujuan yang sesuai, direktori sumber `(source)` ditutup dengan `closedir`. Terakhir, fungsi ini menunggu hingga child process selesai menggunakan `waitpid`.

**Point D**
Setelah mengetahui hewan apa saja yang harus dijaga Grape-kun melakukan zip kepada direktori yang dia buat sebelumnya agar menghemat penyimpanan.

```R
void zipAnimal(){
    id_t child_id;
    int stat;
    if ((child_id = fork()) == 0){
    char *argv[] = {"zip", "-r", "HewanDarat.zip", "HewanDarat", NULL};
    execv("/usr/bin/zip", argv);
    }
    while ((wait(&stat)) > 0)
    ;
    if ((child_id = fork()) == 0){
    char *argv[] = {"zip", "-r", "HewanAir.zip", "HewanAir", NULL};
    execv("/usr/bin/zip", argv);
    }
    while ((wait(&stat)) > 0)
    ;
    if ((child_id = fork()) == 0){
    char *argv[] = {"zip", "-r", "HewanAmphibi.zip", "HewanAmphibi", NULL};
    execv("/usr/bin/zip", argv);
    }
    while ((wait(&stat)) > 0)
    ;
    waitpid(child_id, &stat, 0);
}
```

Script tersebut adalah sebuah fungsi `zipAnimal()` yang berfungsi untuk mengompres direktori `HewanDarat`,` HewanAir`, dan `HewanAmphibi` menjadi file .`zip` masing-masing.
Pada fungsi ini, pertama-tama dilakukan fork untuk memulai proses baru. Kemudian, pada proses child, digunakan perintah `zip` dengan argumen `-r` untuk melakukan rekursif pada direktori yang dijadikan target kompresi. Argumen selanjutnya adalah nama file zip yang akan dihasilkan beserta dengan direktori yang akan di-kompresi.
Setelah perintah selesai dijalankan, proses parent akan menunggu proses child selesai dengan menggunakan `wait()` dan `waitpid()` sehingga setiap proses kompresi harus selesai terlebih dahulu sebelum proses selanjutnya dijalankan.

### Soal 2
Soal 2 secara singkat perlu membuat sebuah program yang berjalan secara daemon, program tersebut akan mendownload gambar dari picsum.photo, kemudian menzipkannya ketika isi folder sudah mencapai 15 buah, dan folder asli dihapus agar tidak memenuhi tempat. Format penamaan adalah timestamp [YYYY-mm-dd_HH:mm:ss]. 

Soal juga meminta untuk dapat menjalankan dua mode dengan argumen '-a' atau '-b' bedanya adalah mode a, akan menghentikan seluruh operasi, sementara mode b, akan menyelesaikan hingga peng-zip-annya. 

Untuk itu, dibuatlah sebuah program killer yang akan menghentikan operasi. 

dari main

`
int main(int argc, char *argv[]){
        if (argc != 2 || (strcmp(argv[1], "-a") != 0 && strcmp(argv[1], "-b") != 0)) {
        printf("Usage: %s [-a|-b]\n", argv[0]);
        return 1;
    }

    pid_t pid, sid;
    pid = fork();
    char url[] = "https://picsum.photos/";

    if (pid > 0) {
        // buat program "killer"
        file_killer(pid, argv[0], argv[1]);

        exit(EXIT_SUCCESS);
    }

    if (pid < 0) {
        exit(EXIT_FAILURE);
    }

    umask(0);
    sid = setsid();

    if (sid < 0) {
        exit(EXIT_FAILURE);
    }

    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);

    while(1){
        struct tm *local;
        time_t t = time(NULL);
        local = localtime(&t);
        char foldername[256];
        strftime(foldername, 30, "%Y-%m-%d_%H:%M:%S", local);
        buat_directory(url,foldername);
         sleep(DELAY_BETWEEN_FOLDER);
    }

    return 0;

}
`

Bagian main, atau pada bagian utama, ialah membuat direktori dengan timestamp setiap 30 detik sebagai proses daemon, dengan memanggil fungsi buat direktori yang berisi

`
void buat_directory(char *url, char foldername[]) {
    pid_t cid;
    cid = fork();

    if (cid < 0) {
        exit(EXIT_FAILURE);
    }
    if (cid == 0) {
    char *argv[] = {"mkdir", "-p", foldername, NULL};
        execv("/bin/mkdir", argv);
    }

    waitpid(cid, NULL, 0);

    pid_t ncid = fork();
    if (ncid < 0) {
        exit(EXIT_FAILURE);
    }
    if (ncid == 0) {

    int i = 0;
        while(1) {
            if(i==MAX_GAMBAR) {
                nge_zip(foldername);
                break;
            }
            
            download_gambar(url, foldername);

            i++;
            sleep(DELAY_BETWEEN_GAMBAR);
        }

        exit(EXIT_SUCCESS);
    }
}
`

program juga akan melakukan download dan zip jika sudah 15, proses pada buat_directory(), download_gambar(), nge_zip(), dibuat menjadi fork dari proses agar dapat berjalan paralel. 

untuk program nge_zip dan download_gambar sendiri adalah sebagai berikut.

`

void nge_zip(char *filename){
    pid_t child_id;
    child_id = fork();

    if (child_id < 0) {
        exit(EXIT_FAILURE);
    }

    if (child_id == 0) {

        char nama_dir[25];
        strcpy(nama_dir, filename);
        strcat(filename, ".zip");

        char *argv[] = {"zip", "-9rmq", filename, nama_dir, NULL};
        execv("/bin/zip", argv);
    }
}


void download_gambar(char url[], char foldername[]){
    pid_t cid = fork();
    char nama_folder[60];
    strcpy(nama_folder, foldername);

    if (cid < 0) {
        exit(EXIT_FAILURE);
    }

    if (cid == 0){
        struct tm *tlocal;
        time_t waktu_dtk = time(NULL);
        tlocal = localtime(&waktu_dtk);

        char filename[25];
        int ukuran = (waktu_dtk%1000)+50;
        char str_ukuran[5];

        snprintf(str_ukuran, 5, "%d", ukuran);
        strcat(url, str_ukuran);
        strftime(filename, 25, "/%Y-%m-%d_%H:%M:%S.jpg", tlocal);
        strcat(foldername, filename);
        
        char *argv[] = {"wget", "-qO", foldername, url, NULL};
            execv("/bin/wget", argv);
    }
}

` 

tinggal kita buatkan program killernya dan compilekan, sehingga siap untuk di execute dari terminal.
pembuatannya adalah sebagai berikut:

`
void execute_killer(char filename[]){
    pid_t cid = fork();

    if (cid < 0) 
    {
        exit(EXIT_FAILURE);
    }
    if (cid == 0){
        char *argv[] = {"gcc", filename, "-o", "killer", NULL};
        execv("/bin/gcc", argv);
    }

    int status;
    wait (&status);
}


void file_killer(int pid_main, char *proses, char *mode_run){
    FILE *fp;
    char filename[] = "killer.c";

    fp= fopen(filename, "w");
    fprintf(fp, "#include <stdio.h>\n#include <stdlib.h>\n#include <signal.h>\n");
    fprintf(fp, "#include <unistd.h>\n#include <sys/types.h>\n#include <sys/wait.h>\n\n");

    fprintf(fp, "void hapus_program(char filename[]) {\n\tpid_t child_id = fork();\n\n");
    fprintf(fp, "\tif (child_id < 0) {\n\t\texit(EXIT_FAILURE);\n\t}\n\n");
    fprintf(fp, "\tif (child_id == 0) {\n\t\tchar *argv[] = {\"rm\", filename, NULL};\n\t\texecv(\"/bin/rm\", argv);\n\t}\n\n");
    fprintf(fp, "\tint status;\n\twait(&status);\n}\n\n");

    if (strcmp(mode_run, "-a") == 0) {
        fprintf(fp, "void pkill(char file_proses[]) {\n\tpid_t child_id= fork();\n\n");
        fprintf(fp, "\tif (child_id < 0) {\n\t\texit(EXIT_FAILURE);\n\t}\n\n");
        fprintf(fp, "\tif (child_id == 0) {\n\t\tchar *argv[] = {\"pkill\", \"-9\", \"-f\", file_proses, NULL};\n\t\texecv(\"/bin/pkill\", argv);\n\t}\n\n");
        fprintf(fp, "\twaitpid(child_id, NULL, 0);\n}\n\n");

        fprintf(fp, "int main() {\n\tpkill(\"%s\");\n\n", proses);
        fprintf(fp, "\thapus_program(\"killer\");\n\t//hapus_program(\"killer.c\");\n\n\treturn 0;\n}");
    } 
    else if(strcmp(mode_run, "-b") == 0) {
        fprintf(fp, "int main() {\n\tint pid = %d;\n\tint terminate = kill(pid, SIGKILL);\n\n", pid_main);
        fprintf(fp, "\thapus_program(\"killer\");\n\t//hapus_program(\"killer.c\");\n\n\treturn 0;\n}");
    }

    fclose(fp);

    execute_killer(filename);

}
`

dengan demikian argumen '-a' atau '-b' akan mempengaruhi file killer yang akan dibuat. apakah akan menunggu, atau langsung dieksekusi.


### Soal 3
Ten Hag adalah seorang pelatih Ajax di Liga Belanda. Suatu hari, Ten Hag mendapatkan tawaran untuk menjadi manajer Manchester United. Karena Ten Hag masih mempertimbangkan tawaran tersebut, ia ingin mengenal para pemain yang akan dilatih kedepannya. Dikarenakan Ten Hag hanya mendapatkan url atau link database mentah para pemain bola, maka ia perlu melakukan klasifikasi pemain Manchester United. Bantulah Ten Hag untuk mengenal para pemain Manchester United tersebut hanya dengan 1 Program C bernama “filter.c”

A. Pertama-tama, Program filter.c akan mengunduh file yang berisikan database para pemain bola. Kemudian dalam program yang sama diminta dapat melakukan extract “players.zip”. Lalu hapus file zip tersebut agar tidak memenuhi komputer Ten Hag.

B. Dikarenakan database yang diunduh masih data mentah. Maka bantulah Ten Hag untuk menghapus semua pemain yang bukan dari Manchester United yang ada di directory.

C. Setelah mengetahui nama-nama pemain Manchester United, Ten Hag perlu untuk mengkategorikan pemain tersebut sesuai dengan posisi mereka dalam waktu bersamaan dengan 4 proses yang berbeda. Untuk kategori folder akan menjadi 4 yaitu Kiper, Bek, Gelandang, dan Penyerang.

D. Setelah mengkategorikan anggota tim Manchester United, Ten Hag memerlukan Kesebelasan Terbaik untuk menjadi senjata utama MU berdasarkan rating terbaik dengan wajib adanya kiper, bek, gelandang, dan penyerang. (Kiper pasti satu pemain). Untuk output nya akan menjadi Formasi\_[jumlah bek]-[jumlah gelandang]-[jumlah penyerang].txt dan akan ditaruh di /home/[users]/

Berikut adalah jawabannya

```c
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <dirent.h>
#include <sys/stat.h>
#include <string.h>
#define ZIP_FILENAME "players.zip"
```

Kode di atas adalah definisi beberapa header file yang digunakan dalam program untuk melakukan beberapa operasi seperti manajemen direktori, penggunaan process, dan input/output. ZIP_FILENAME adalah konstanta yang didefinisikan untuk menentukan nama file zip yang akan didownload.

```c
void delete_png_files(char *dir_name) {
    DIR *dir;
    struct dirent *entry;
    char file_path[1000];

    if ((dir = opendir(dir_name)) != NULL) {
        while ((entry = readdir(dir)) != NULL) {
            if (entry->d_type == DT_REG && strstr(entry->d_name, ".png") != NULL) {
                if (strstr(entry->d_name, "ManUtd") == NULL) {
                    sprintf(file_path, "%s/%s", dir_name, entry->d_name);
                    if (remove(file_path) == 0) {
                        printf("File %s deleted successfully\n", entry->d_name);
                    } else {
                        printf("Error: Unable to delete file %s\n", entry->d_name);
                    }
                }
            }
        }
        closedir(dir);
    } else {
        printf("Error: Unable to open directory %s\n", dir_name);
    }
}
```

Fungsi `delete_png_files` digunakan untuk menghapus semua file dengan ekstensi .png dalam suatu direktori, kecuali file yang memiliki string "ManUtd" di dalam namanya. Fungsi ini membuka direktori dengan opendir, dan memeriksa setiap file di dalamnya dengan readdir. Jika file adalah file regular dan memiliki ekstensi .png dan tidak mengandung string "ManUtd", maka fungsi akan menghapus file tersebut dengan menggunakan remove. Jika file tidak dapat dihapus, fungsi akan menampilkan pesan error. Setelah semua file dihapus, fungsi akan menutup direktori dengan closedir. Jika direktori tidak dapat dibuka, fungsi akan menampilkan pesan error.

```c
void categorize_players(char *dir_name) {
    DIR *dir;
    struct dirent *entry;
    char file_path[1000], cmd[1000];

    if ((dir = opendir(dir_name)) != NULL) {
        while ((entry = readdir(dir)) != NULL) {
            if (entry->d_type == DT_REG && strstr(entry->d_name, ".png") != NULL) {
                char *file_name = entry->d_name;
                char player_name[100], player_team[100], player_position[100];
                int player_rating;

                // Parse player name, team, position, and rating from file name
                sscanf(file_name, "%[^_]_%[^_]_%[^_]_%d.png", player_name, player_team, player_position, &player_rating);

                // Build directory path based on player position
                char dir_path[1000];
                if (strcmp(player_position, "Kiper") == 0) {
                    sprintf(dir_path, "%s/%s", dir_name, "Kiper");
                } else if (strcmp(player_position, "Bek") == 0) {
                    sprintf(dir_path, "%s/%s", dir_name, "Bek");
                } else if (strcmp(player_position, "Gelandang") == 0) {
                    sprintf(dir_path, "%s/%s", dir_name, "Gelandang");
                } else if (strcmp(player_position, "Penyerang") == 0) {
                    sprintf(dir_path, "%s/%s", dir_name, "Penyerang");
                } else {
                    printf("Error: Unknown player position %s\n", player_position);
                    continue;
                }
                // Create directory for player position if it doesn't exist
if (mkdir(dir_path, 0755) == -1) {
    if (errno != EEXIST) {
        printf("Error: Unable to create directory %s\n", dir_path);
        continue;
    }
}

// Move file to appropriate directory
sprintf(file_path, "%s/%s", dir_name, entry->d_name);
sprintf(new_file_path, "%s/%s", dir_path, entry->d_name);
if (rename(file_path, new_file_path) == -1) {
    printf("Error: Unable to move file %s to directory %s\n", entry->d_name, dir_path);
    continue;
} else {
    printf("File %s moved to directory %s successfully\n", entry->d_name, dir_path);
}

            }
        }
        closedir(dir);
    } else {
        printf("Error: Unable to open directory %s\n", dir_name);
    }
}
```

Fungsi `categorize_players` digunakan untuk memindahkan file gambar pemain ke dalam direktori yang sesuai dengan posisi pemain tersebut. Fungsi ini membuka direktori yang diberikan sebagai argumen, mencari semua file dengan ekstensi ".png", dan memindahkan file-file tersebut ke dalam direktori yang sesuai dengan posisi pemain dalam nama file. Jika direktori yang sesuai dengan posisi pemain belum ada, fungsi akan membuat direktori tersebut terlebih dahulu. Jika terdapat error dalam pembuatan direktori atau pemindahan file, fungsi akan mencetak pesan error.

```c
typedef struct {
  int rating;
  char filename[300];
} Pemain;

int bandingkan_rate(const void* a, const void* b) {
  const Pemain* pemain_a = (const Pemain*)a;
  const Pemain* pemain_b = (const Pemain*)b;
  return pemain_b->rating - pemain_a->rating;
}

void ambil_rate_player(const char* position, Pemain* pemain, int* count) {
  struct dirent* entry; DIR* dir;
  char directory[100];

  sprintf(directory,"players/%s", position);
  dir = opendir(directory);

	if (dir) {
	    for (entry = readdir(dir); entry; entry = readdir(dir)) {
		if (strstr(entry->d_name, ".png") && entry->d_type == DT_REG) {
		    int rating;
		    sscanf(entry->d_name, "%*[^_]_%*[^_]_%*[^_]_%d.png", &rating);
		    strcpy(pemain[*count].filename, entry->d_name);
		    pemain[*count].rating = rating;
		    (*count)++;
		}
	    }
	    closedir(dir);
	} else {
	    exit(EXIT_FAILURE);
	}
}

void BuatTim(int bek, int gelandang, int penyerang) {
    Pemain pemain_gelandang[100], pemain_penyerang[100], pemain_kiper[100], pemain_bek[100];
    int jumlah_gelandang = 0, jumlah_penyerang = 0, jumlah_kiper = 0, jumlah_bek = 0;
    ambil_rate_player("Gelandang", pemain_gelandang, &jumlah_gelandang);
    ambil_rate_player("Penyerang", pemain_penyerang, &jumlah_penyerang);
    ambil_rate_player("Kiper",pemain_kiper, &jumlah_kiper);
    ambil_rate_player("Bek", pemain_bek, &jumlah_bek);

    if (bek > jumlah_bek || gelandang > jumlah_gelandang || penyerang > jumlah_penyerang) {
        printf("Jumlah pemain tidak mencukupi\n");
        return;
    }
  qsort(pemain_gelandang, jumlah_gelandang, sizeof(Pemain), bandingkan_rate);
  qsort(pemain_penyerang, jumlah_penyerang, sizeof(Pemain), bandingkan_rate);
  qsort(pemain_bek, jumlah_bek, sizeof(Pemain), bandingkan_rate);
  qsort(pemain_kiper, jumlah_kiper, sizeof(Pemain), bandingkan_rate);

  char filepath[300];
  sprintf(filepath,"Formasi_%d-%d-%d.txt", bek, gelandang, penyerang);
  FILE* file = fopen(filepath, "w");

  if (file) {
  	int i;
	    fprintf(file, "Posisi Kiper:\n%s\n\n", pemain_kiper[i].filename);

	    fprintf(file, "Posisi Bek:\n");
	    for (i = 0; i < bek && i < jumlah_bek; i++) {
	      fprintf(file, "%d. %s\n",i+1, pemain_bek[i].filename);
	    }

	    fprintf(file, "\nPosisi Gelandang:\n");
	    for (i = 0; i < gelandang && i < jumlah_gelandang; i++) {
	      fprintf(file, "%d. %s\n",i+1, pemain_gelandang[i].filename);
	    }

	    fprintf(file, "\nPosisi Penyerang:\n");
	    for (i = 0; i < penyerang && i < jumlah_penyerang; i++) {
	      fprintf(file, "%d. %s\n",i+1, pemain_penyerang[i].filename);
	    }

	    fclose(file);
	    printf("Lineup saved\n");
  } else {
	    printf("Error\n");
	    exit(EXIT_FAILURE);
  }
}
```

`bandingkan_rate()`: fungsi ini digunakan sebagai penghitung perbandingan rating pemain untuk digunakan dalam fungsi sorting qsort().

`ambil_rate_player()`: fungsi ini digunakan untuk mengambil rating dari setiap file pemain dalam folder tertentu, dan menyimpannya dalam array Pemain.

`BuatTim()`: fungsi utama yang digunakan untuk membuat formasi tim sepak bola. Fungsi ini akan memanggil fungsi ambil_rate_player() untuk mengambil rating dari setiap pemain dalam folder masing-masing. Kemudian, fungsi akan menggunakan fungsi sorting qsort() untuk mengurutkan pemain-pemain tersebut berdasarkan rating mereka. Terakhir, fungsi akan menuliskan formasi tim yang telah dibuat ke dalam file .txt.

```c
int main(int argc, char *argv[]) {
    char *url = "https://drive.google.com/u/0/uc?id=1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF&export=download";
    char *zip_dir = ".";
    char zip_file[1000];
    sprintf(zip_file, "%s/%s", zip_dir, ZIP_FILENAME);
    	if (argc < 3) {
        printf("Usage: %s [bek] [gelandang] [penyerang]\n", argv[0]);
        exit(EXIT_FAILURE);

}

    pid_t pid = fork();
    if (pid == -1) {
        printf("Error forking process\n");
        return -1;
    } else if (pid == 0) {
        execl("/usr/bin/wget", "wget", "-O", zip_file, url, NULL);
        exit(127);
    } else {
        int status;
        waitpid(pid, &status, 0);
        if (WIFEXITED(status) && WEXITSTATUS(status) == 0) {
            pid = fork();
            if (pid == -1) {
                printf("Error forking process\n");
                return -1;
            } else if (pid == 0) {
                execl("/usr/bin/unzip", "unzip", "-q", zip_file, "-d", zip_dir, NULL);
                exit(127);
            } else {
                waitpid(pid, &status, 0);
                if (WIFEXITED(status) && WEXITSTATUS(status) == 0) {
                    delete_png_files("players");
                    categorize_players("players");
		            int bek,gelandang,penyerang;
                    BuatTim(atoi(argv[1]),atoi(argv[2]),atoi(argv[3]));
                    if (remove(zip_file) != 0) {
                    printf("Error deleting zip file\n");
                    return -1;
            } else {
    printf("ZIP file deleted successfully\n");
}
            } else {
            printf("Error extracting zip file\n");
            return -1;
        }
    }
    } else {
    printf("Error downloading file\n");
    return -1;
    }
    }
    return 0;
}
```

Ini adalah main dari kode ini

```c
if (argc < 3) {
printf("Usage: %s [bek] [gelandang] [penyerang]\n", argv[0]);
exit(EXIT_FAILURE);
```

Kode ini memeriksa jumlah argumen yang diberikan saat menjalankan program. Jika jumlahnya kurang dari 3, maka program akan menampilkan pesan penggunaan dan keluar dari program dengan kode keluar EXIT_FAILURE. Pesan penggunaan ini berisi format argumen yang benar yang harus diberikan untuk menjalankan program.

```c
pid_t pid = fork();
    if (pid == -1) {
        printf("Error forking process\n");
        return -1;
    } else if (pid == 0) {
        execl("/usr/bin/wget", "wget", "-O", zip_file, url, NULL);
        exit(127);
    } else {
        int status;
        waitpid(pid, &status, 0);
        if (WIFEXITED(status) && WEXITSTATUS(status) == 0) {
            pid = fork();
            if (pid == -1) {
                printf("Error forking process\n");
                return -1;
            } else if (pid == 0) {
                execl("/usr/bin/unzip", "unzip", "-q", zip_file, "-d", zip_dir, NULL);
                exit(127);
            } else {
                waitpid(pid, &status, 0);
                if (WIFEXITED(status) && WEXITSTATUS(status) == 0) {
                    delete_png_files("players");
                    categorize_players("players");
		            int bek,gelandang,penyerang;
                    BuatTim(atoi(argv[1]),atoi(argv[2]),atoi(argv[3]));
                    if (remove(zip_file) != 0) {
                    printf("Error deleting zip file\n");
                    return -1;
            } else {
    printf("ZIP file deleted successfully\n");
}
            } else {
            printf("Error extracting zip file\n");
            return -1;
        }
    }
    } else {
    printf("Error downloading file\n");
    return -1;
    }
    }
    return 0;
}
```

Kode di atas adalah bagian utama program yang melakukan proses download, extract, kategorisasi pemain, dan pembuatan formasi tim. Pada bagian awal program, diperiksa apakah program dijalankan dengan argumen yang benar, yaitu jumlah pemain untuk posisi bek, gelandang, dan penyerang. Jika tidak benar, program akan menampilkan pesan usage dan keluar dengan status EXIT_FAILURE.

Selanjutnya, program melakukan proses download file dari Google Drive menggunakan wget dan menyimpannya dengan nama ZIP_FILENAME di direktori yang ditentukan oleh zip_dir. Setelah download selesai, program mengekstrak file ZIP tersebut menggunakan unzip ke direktori yang sama.

Setelah proses extract selesai, program memanggil dua fungsi, yaitu delete_png_files() dan categorize_players(), yang bertugas menghapus file PNG yang tidak sesuai dan mengkategorisasi pemain ke dalam folder yang sesuai berdasarkan posisi pemain.

Kemudian, program memanggil fungsi BuatTim() dengan parameter jumlah pemain untuk setiap posisi, yaitu bek, gelandang, dan penyerang, yang berfungsi untuk membentuk formasi tim berdasarkan pemain yang tersedia dan kemudian menyimpannya dalam file txt.

Terakhir, program menghapus file ZIP yang telah diunduh menggunakan fungsi remove(), kemudian menampilkan pesan sukses jika penghapusan file ZIP berhasil. Jika terdapat error selama proses download, extract, atau proses lainnya, program akan menampilkan pesan error dan keluar dengan status -1. Jika semua proses berhasil dilakukan, program akan keluar dengan status 0.

### Untuk code full

```c
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <dirent.h>
#include <sys/stat.h>
#include <string.h>

#define ZIP_FILENAME "players.zip"

void delete_png_files(char *dir_name) {
    DIR *dir;
    struct dirent *entry;
    char file_path[1000];

    if ((dir = opendir(dir_name)) != NULL) {
        while ((entry = readdir(dir)) != NULL) {
            if (entry->d_type == DT_REG && strstr(entry->d_name, ".png") != NULL) {
                if (strstr(entry->d_name, "ManUtd") == NULL) {
                    sprintf(file_path, "%s/%s", dir_name, entry->d_name);
                    if (remove(file_path) == 0) {
                        printf("File %s deleted successfully\n", entry->d_name);
                    } else {
                        printf("Error: Unable to delete file %s\n", entry->d_name);
                    }
                }
            }
        }
        closedir(dir);
    } else {
        printf("Error: Unable to open directory %s\n", dir_name);
    }
}

void categorize_players(char *dir_name) {
    DIR *dir;
    struct dirent *entry;
    char file_path[1000], cmd[1000];

    if ((dir = opendir(dir_name)) != NULL) {
        while ((entry = readdir(dir)) != NULL) {
            if (entry->d_type == DT_REG && strstr(entry->d_name, ".png") != NULL) {
                char *file_name = entry->d_name;
                char player_name[100], player_team[100], player_position[100];
                int player_rating;

                // Parse player name, team, position, and rating from file name
                sscanf(file_name, "%[^_]_%[^_]_%[^_]_%d.png", player_name, player_team, player_position, &player_rating);

                // Build directory path based on player position
                char dir_path[1000];
                if (strcmp(player_position, "Kiper") == 0) {
                    sprintf(dir_path, "%s/%s", dir_name, "Kiper");
                } else if (strcmp(player_position, "Bek") == 0) {
                    sprintf(dir_path, "%s/%s", dir_name, "Bek");
                } else if (strcmp(player_position, "Gelandang") == 0) {
                    sprintf(dir_path, "%s/%s", dir_name, "Gelandang");
                } else if (strcmp(player_position, "Penyerang") == 0) {
                    sprintf(dir_path, "%s/%s", dir_name, "Penyerang");
                } else {
                    printf("Error: Unknown player position %s\n", player_position);
                    continue;
                }

              if (mkdir(dir_path, 0755) == -1) {
    if (errno != EEXIST) {
        printf("Error: Unable to create directory %s\n", dir_path);
        continue;
    }
}

// Move file to appropriate directory
sprintf(file_path, "%s/%s", dir_name, entry->d_name);
sprintf(new_file_path, "%s/%s", dir_path, entry->d_name);
if (rename(file_path, new_file_path) == -1) {
    printf("Error: Unable to move file %s to directory %s\n", entry->d_name, dir_path);
    continue;
} else {
    printf("File %s moved to directory %s successfully\n", entry->d_name, dir_path);
}
            }
        }
        closedir(dir);
    } else {
        printf("Error: Unable to open directory %s\n", dir_name);
    }
}
typedef struct {
  int rating;
  char filename[300];
} Pemain;

int bandingkan_rate(const void* a, const void* b) {
  const Pemain* pemain_a = (const Pemain*)a;
  const Pemain* pemain_b = (const Pemain*)b;
  return pemain_b->rating - pemain_a->rating;
}

void ambil_rate_player(const char* position, Pemain* pemain, int* count) {
  struct dirent* entry; DIR* dir;
  char directory[100];

  sprintf(directory,"players/%s", position);
  dir = opendir(directory);

	if (dir) {
	    for (entry = readdir(dir); entry; entry = readdir(dir)) {
		if (strstr(entry->d_name, ".png") && entry->d_type == DT_REG) {
		    int rating;
		    sscanf(entry->d_name, "%*[^_]_%*[^_]_%*[^_]_%d.png", &rating);
		    strcpy(pemain[*count].filename, entry->d_name);
		    pemain[*count].rating = rating;
		    (*count)++;
		}
	    }
	    closedir(dir);
	} else {
	    exit(EXIT_FAILURE);
	}
}

void BuatTim(int bek, int gelandang, int penyerang) {
    Pemain pemain_gelandang[100], pemain_penyerang[100], pemain_kiper[100], pemain_bek[100];
    int jumlah_gelandang = 0, jumlah_penyerang = 0, jumlah_kiper = 0, jumlah_bek = 0;
    ambil_rate_player("Gelandang", pemain_gelandang, &jumlah_gelandang);
    ambil_rate_player("Penyerang", pemain_penyerang, &jumlah_penyerang);
    ambil_rate_player("Kiper",pemain_kiper, &jumlah_kiper);
    ambil_rate_player("Bek", pemain_bek, &jumlah_bek);

    if (bek > jumlah_bek || gelandang > jumlah_gelandang || penyerang > jumlah_penyerang) {
        printf("Jumlah pemain tidak mencukupi\n");
        return;
    }
  qsort(pemain_gelandang, jumlah_gelandang, sizeof(Pemain), bandingkan_rate);
  qsort(pemain_penyerang, jumlah_penyerang, sizeof(Pemain), bandingkan_rate);
  qsort(pemain_bek, jumlah_bek, sizeof(Pemain), bandingkan_rate);
  qsort(pemain_kiper, jumlah_kiper, sizeof(Pemain), bandingkan_rate);

  char filepath[300];
  sprintf(filepath,"Formasi_%d-%d-%d.txt", bek, gelandang, penyerang);
  FILE* file = fopen(filepath, "w");

  if (file) {
  	int i;
	    fprintf(file, "Posisi Kiper:\n%s\n\n", pemain_kiper[i].filename);

	    fprintf(file, "Posisi Bek:\n");
	    for (i = 0; i < bek && i < jumlah_bek; i++) {
	      fprintf(file, "%d. %s\n",i+1, pemain_bek[i].filename);
	    }

	    fprintf(file, "\nPosisi Gelandang:\n");
	    for (i = 0; i < gelandang && i < jumlah_gelandang; i++) {
	      fprintf(file, "%d. %s\n",i+1, pemain_gelandang[i].filename);
	    }

	    fprintf(file, "\nPosisi Penyerang:\n");
	    for (i = 0; i < penyerang && i < jumlah_penyerang; i++) {
	      fprintf(file, "%d. %s\n",i+1, pemain_penyerang[i].filename);
	    }

	    fclose(file);
	    printf("Lineup saved\n");
  } else {
	    printf("Error\n");
	    exit(EXIT_FAILURE);
  }
}



int main(int argc, char *argv[]) {
    char *url = "https://drive.google.com/u/0/uc?id=1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF&export=download";
    char *zip_dir = ".";
    char zip_file[1000];
    sprintf(zip_file, "%s/%s", zip_dir, ZIP_FILENAME);
    	if (argc < 3) {
        printf("Usage: %s [bek] [gelandang] [penyerang]\n", argv[0]);
        exit(EXIT_FAILURE);

}

    pid_t pid = fork();
    if (pid == -1) {
        printf("Error forking process\n");
        return -1;
    } else if (pid == 0) {
        execl("/usr/bin/wget", "wget", "-O", zip_file, url, NULL);
        exit(127);
    } else {
        int status;
        waitpid(pid, &status, 0);
        if (WIFEXITED(status) && WEXITSTATUS(status) == 0) {
            pid = fork();
            if (pid == -1) {
                printf("Error forking process\n");
                return -1;
            } else if (pid == 0) {
                execl("/usr/bin/unzip", "unzip", "-q", zip_file, "-d", zip_dir, NULL);
                exit(127);
            } else {
                waitpid(pid, &status, 0);
                if (WIFEXITED(status) && WEXITSTATUS(status) == 0) {
                    delete_png_files("players");
                    categorize_players("players");
		    int bek,gelandang,penyerang;
    BuatTim(atoi(argv[1]),atoi(argv[2]),atoi(argv[3]));
                    if (remove(zip_file) != 0) {
                    printf("Error deleting zip file\n");
                    return -1;
            } else {
    printf("ZIP file deleted successfully\n");
}
            } else {
            printf("Error extracting zip file\n");
            return -1;
        }
    }
    } else {
    printf("Error downloading file\n");
    return -1;
    }
    }
    return 0;
}
```


### Soal 4




## Getting started

Dari sekian banyak hal yang bisa dikerjakan, tak banyak yang bisa dikumpulkan pada pengumpulan pertamanya. Mungkin akan lebih baik pada pengerjaan di revisinya.

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/sisop-c04/sisop-praktikum-modul-2-2023-am-c04.git
git branch -M main
git push -uf origin main
```
